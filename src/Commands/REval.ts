/**
 * @license
 *
 * EVX-Bot
 * Copyright (C) 2019  VoidNull
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Command from '../Structures/Command';
import Eris from 'eris';
import Client from '../Structures/Client';
import superagent from 'superagent';

export default class REval extends Command {
    constructor(client: Eris.CommandClient, base: Client) {
        super(client, base);
        this.label = 'reval';
        this.aliases = ['re', 'remoteeval'];
        this.permissionsNeeded = ['sendMessages'];

        this.ownerOnly = true;
        this.options = {
            argsRequired: true,
            description: 'Remotely Eval code on the instance',
            usage: 'reval [code]',
            fullDescription: 'Eval some JavaScript code on the instances side, instance owner only',
        };
    }

    canRegister(): boolean | Promise<boolean> {
        if (!this.base || !this.base.config || !this.base.config.owner || !this.base.config.owner.discordID || !this.base.config.owner.tokenAuth) {
            return false;
        }
        if (!this.base.config.url) {
            return false;
        }
        return super.canRegister();
    }

    async execute(msg: Eris.Message, args: string[] ): Promise<Eris.Message|void> {
        if (!this.base || !this.base.config.owner || !this.base.config.owner.tokenAuth) {
            return msg.channel.createMessage('Internal Error Occurred.');
        }
        try {
            const pair = this.base.config.owner.tokenAuth.split(':');
            const out = await superagent.post(`${this.base.config.url}/api/eval`).set( { uid: pair[0], token: pair[1] } ).send( { eval: args.join(' ') } );
            const okCode = 200;
            if (out.status === okCode && out.text) {
                const discordMessageLimit = 2000;
                if (out.text.length > discordMessageLimit) {
                    return msg.channel.createMessage('Output too big');
                }
                return msg.channel.createMessage(`\`\`\`js\n${out.text}\`\`\``);
            }
            if (out.status === okCode && !out.text) {
                return msg.channel.createMessage('Output too big');
            }
            if (out.noContent) {
                return msg.channel.createMessage('No output.');
            }
            if (out.notFound) {
                return msg.channel.createMessage('URL Not found! Perhaps the instance is not up to date?');
            }
            if (out.badRequest) {
                return msg.channel.createMessage(`Something failed when generating the request.`);
            }
            if (out.unauthorized) {
                return msg.channel.createMessage('UNAUTHORIZED!');
            }
            return msg.channel.createMessage(`Something went wrong.\nPossibly invalid instance or instance doesn't have eval.`);
        } catch (err) {
            if (err.response && err.response.notFound) {
                return msg.channel.createMessage('URL Not found! Perhaps the instance is not up to date?');
            }
            if (err.response && err.response.badRequest) {
                return msg.channel.createMessage(`Something failed when generating the request.`);
            }
            if (err.response && err.response.unauthorized) {
                return msg.channel.createMessage('UNAUTHORIZED!');
            }
            console.log(err.stack);
            return msg.channel.createMessage(`Something went wrong. See console for details and below for a short message.\n\`\`\`js\n${err}\`\`\``);
        }
    }
}
