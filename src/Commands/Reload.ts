/**
 * @license
 *
 * EVX-Bot
 * Copyright (C) 2019  VoidNull
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Command from '../Structures/Command';
import Eris, { CommandClient, Message } from 'eris';
import Client from '../Structures/Client';
import { promisify } from 'util';
import cp from 'child_process';
const exec = promisify(cp.exec);

export default class Reload extends Command {
    constructor(client: CommandClient, base: Client) {
        super(client, base);
        this.label = 'reload';
        this.ownerOnly = true;

        this.options = {
            description: 'Reload a command',
            fullDescription: 'Reload a command, by command name',
            usage: 'reload [command name] (-tsc|-u|-update)',
            argsRequired: true,
        };
    }

    listCommands(): boolean|Command[] {
        if (!this.base) {
            return false;
        }
        const commands: Command[] = [];
        this.base.commands.forEach(command => commands.push(command) );
        return commands;
    }

    findCommand(argument: string): Command | boolean {
        if (!this.base) {
            return false;
        }
        const command = this.base.commands.get(argument.toLowerCase() );
        if (!command) {
            const commands = this.listCommands();
            if (!commands) {
                return false;
            }
            if (typeof commands !== 'boolean') {
                const scmd = commands.find(acommand => acommand.aliases.includes(argument.toLowerCase() ) );
                if (!scmd) {
                    return false;
                }
                return scmd;
            }
            return false;
        }
        return command;
    }

    async execute(msg: Eris.Message, args: string[] ): Promise<void|Message|string> {
        if (!this.base) {
            return 'Internal Error';
        }
        if (!args[0] ) {
            return 'Please provide a command to reload';
        }
        const command = this.findCommand(args[0] );
        if (!command || !(command instanceof Command) ) {
            return 'Command not found';
        }
        if (command.label === this.label) {
            return 'The reload command cannot be reloaded while running, you must restart the instance.';
        }
        if (args[1] && args[1].match(/-u|-tsc|--update/) ) {
            const mess = await msg.channel.createMessage('Running typescript transpiler...');
            await exec(`cd ${process.cwd()} && tsc`);
            mess.delete();
        }
        delete require.cache[command.path];
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const Cmd = require(command.path).default;
        const cmd = new Cmd(this.client, this.base);
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        this.client.unregisterCommand(cmd.label);
        this.base.reregisterCommand(cmd);
        return msg.channel.createMessage(`Reloaded command \`${command.label}\`!`);
    }
}
