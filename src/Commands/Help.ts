/**
 * @license
 *
 * EVX-Bot
 * Copyright (C) 2019  VoidNull
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Command from '../Structures/Command';
import Eris, { CommandClient, Message, MessageContent } from 'eris';
import Client from '../Structures/Client';

interface HelpMess {
    embed: {
        title: string;
        description: string;
        fields: { name: string; value: string; inline?: boolean }[];
        color: number;
    };
}

export default class Help extends Command {
    constructor(client: CommandClient, base: Client) {
        super(client, base);
        this.label = 'help';
        this.options = { description: 'This help text', fullDescription: 'This help text' };
    }

    findCommand(argument: string): Eris.Command | boolean {
        const command = this.client.commands[argument.toLowerCase()];
        if (!command) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
            // @ts-ignore
            const alias = this.client.commandAliases[argument.toLowerCase()];
            if (!alias) {
                return false;
            }
            return this.client.commands[alias.toLowerCase()];
        }
        return command;
    }

    findSubcommand(command: Eris.Command, argument: string): Eris.Command|boolean {
        const subcommand = command.subcommands[argument.toLowerCase()];
        if (!subcommand) {
            const alias = command.subcommandAliases[argument.toLowerCase()];
            if (!alias) {
                return false;
            }
            // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
            // @ts-ignore
            return command.subcommands[alias];
        }
        return subcommand;
    }

    execute(msg: Message, args: string[] ): Promise<Message> {
        if (args[0] && args[0].toLowerCase() !== 'help') {
            if (!this.base) {
                return msg.channel.createMessage('Internal Error');
            }
            const command = this.findCommand(args[0] );
            if (!command || typeof command === 'boolean') {
                return msg.channel.createMessage('Command not found!');
            }
            if (command && args[1] ) {
                console.log(args[1] );
                const subcommand = this.findSubcommand(command, args[1] );
                if (!subcommand || typeof subcommand === 'boolean') {
                    return this.execute(msg, [args[0]] );
                }
                const mess: HelpMess = {
                    embed: {
                        title: `Help ${command.label} ${subcommand.label}`,
                        description: `${subcommand.fullDescription || subcommand.description || '[MISSING]'}`,
                        fields: [],
                        color: Number('0x15959D'),
                    },
                };
                if (subcommand.cooldown) {
                    mess.embed.fields.push( { name: 'Cooldown', value: `${subcommand.cooldown / 1000}` } );
                }
                if (subcommand.usage) {
                    mess.embed.fields.push( { name: 'Usage', value: subcommand.usage } );
                }
                const res = [];
                if (subcommand.dmOnly) {
                    res.push('DM Only');
                }
                const cmd = this.base.commands.get(command.label);
                if (cmd) {
                    const subcmd = cmd.asubcommands.find(s => s.label === args[1].toLowerCase() );
                    if (!subcmd) {
                        return msg.channel.createMessage('Something wen\'t wrong');
                    }
                    if (subcmd.ownerOnly) {
                        res.push('Owner Only');
                    }
                }
                if (res.length > 0) {
                    mess.embed.fields.push( { name: 'Restrictions', value: res.join('\n') } );
                }
                return msg.channel.createMessage(mess as MessageContent);
            }
            const mess: HelpMess = {
                embed: {
                    title: `Help: ${command.label}`,
                    description: `${command.fullDescription || command.description || '[MISSING]'}`,
                    fields: [],
                    color: Number('0x15959D'),
                },
            };
            if (command.cooldown) {
                mess.embed.fields.push( { name: 'Cooldown', value: `${command.cooldown / 1000}s` } );
            }
            if (command.usage) {
                mess.embed.fields.push( { name: 'Usage', value: command.usage } );
            }
            const res = [];
            if (command.dmOnly) {
                res.push('DM Only');
            }
            const cmd = this.base.commands.get(command.label);
            if (cmd && cmd.ownerOnly) {
                res.push('Owner Only');
            }
            if (res.length > 0) {
                mess.embed.fields.push( { name: 'Restrictions', value: res.join('\n') } );
            }
            return msg.channel.createMessage(mess as MessageContent);
        }
        let cmdStr = '';
        let cmds = false;
        for (const fcommand in this.client.commands) {
            const command = this.client.commands[fcommand];
            if (!command.hidden) {
                if (cmds) {
                    cmdStr += `\n**${command.label}** - ${command.description || '[MISSING]'}`;
                } else {
                    cmdStr += `**${command.label}** - ${command.description || '[MISSING]'}`;
                    cmds = true;
                }
            }
        }
        return msg.channel.createMessage( {
            embed: {
                title: 'Help',
                description: cmdStr,
                color: Number('0x15959D'),
            },
        } );
    }
}
