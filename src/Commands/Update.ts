/**
 * @license
 *
 * EVX-Bot
 * Copyright (C) 2019  VoidNull
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Command from '../Structures/Command';
import { CommandClient, Message } from 'eris';
import cp from 'child_process';
import { promisify } from 'util';
import Client from '../Structures/Client';

const exec = promisify(cp.exec);

export default class Update extends Command {
    constructor(client: CommandClient, base: Client) {
        super(client, base);
        this.label = 'update';
        this.ownerOnly = true;

        this.options = {
            fullDescription: '(Attempt to) Update the bot via GIT & transpile the code',
            description: 'Update the bot',
        };
    }

    canRegister(): boolean | Promise<boolean> {
        if (!this.base || !this.base.config || !this.base.config.owner || !this.base.config.owner.discordID) {
            return false;
        }
        this.options.requirements = {
            userIDs: [this.base.config.owner.discordID],
        };
        return true;
    }

    async execute(msg: Message): Promise<Message|void> {
        let branch: any = await exec('git branch');
        branch = branch.stdout;
        branch = branch.split('\n');
        branch = branch.find( (arr: string) => arr.startsWith('*') ).replace('* ', '');
        let vers: string | { stderr: string; stdout: string } = await exec('git log -1 --oneline');
        vers = vers.stdout;
        await exec(`git fetch origin ${branch}`);
        let upstream: string | { stderr: string; stdout: string } = await exec('git rev-parse @{u}');
        upstream = upstream.stdout;
        let local: string | { stderr: string; stdout: string } = await exec('git rev-parse @{0}');
        local = local.stdout;
        let base: string | { stderr: string; stdout: string } = await exec('git merge-base @{0} @{u}');
        base = base.stdout;
        let status: string | { stderr: string; stdout: string } = await exec('git status --porcelain=2');
        status = status.stdout;

        for (const eh of status.split('\n') ) {
            if (eh.match(/^\d+ M\.|^\d+ M|^\d+ \.M/) ) {
                return msg.channel.createMessage('There are modified files scheduled for committing. Cannot update.');
            }
            if (eh.match(/^\d+ D\.|^\d+ D|^\d+ \.D/) ) {
                return msg.channel.createMessage('Evolve-X-Bot has deleted files scheduled for committing. Cannot update.');
            }
            if (eh.match(/^\d+ R([M.])|^\d+ R[M.]|^\d+ \.R[M.]/) ) {
                return msg.channel.createMessage('Evolve-X-Bot has renamed files scheduled for committing. Cannot update');
            }
        }

        if (local === upstream) {
            return msg.channel.createMessage('Up to date');
        }
        if (base === upstream) {
            return msg.channel.createMessage('Ahead of upstream');
        }
        if (base === local) {
            // Do nothing
        } else {
            return msg.channel.createMessage('Diverged');
        }

        let hash;
        if (vers) {
            if (vers.match(/\(\)/) ) {
                const eh = vers.split(/\s\(|\)\s/);
                hash = eh[0];
            } else {
                const eh = vers.split(' ');
                hash = eh[0];
            }
        }

        const mess = await msg.channel.createMessage(`Updating from ${hash} to ${upstream.slice(0, 7)}...`);
        let update = true;
        let updMess = 'null';

        try {
            const pull = await exec(`git pull origin ${branch}`);
            updMess = pull.stdout;
            await exec(`cd ${process.cwd()} && tsc`);
        } catch (err) {
            update = false;
            let mess = err.message || err;
            mess = mess.split('\n');
            const aMess = [];
            for (const mes of mess) {
                if (!mes.startsWith('Command Failed:') && !mes.startsWith('From https://') && !mes.startsWith(' * branch') && mess !== '\n') {
                    aMess.push(mes);
                }
            }
            updMess = aMess.join('\n');
        }

        return mess.edit(`${!update ? 'Update failed. Here is why:' : 'Update success! Git message:'} \`\`\`${updMess}\`\`\`${!update ? 'You will have to manually update Evolve-X-Bot, or stash your changes.' : 'Restart Evolve-X-Bot to see these changes take place.'}`);
    }
}
