/**
 * @license
 *
 * EVX-Bot
 * Copyright (C) 2019  VoidNull
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import superagent from 'superagent';
import Command from '../Structures/Command';
import Eris, { CommandClient } from 'eris';
import Client from '../Structures/Client';
import moment from 'moment';

export default class Info extends Command {
    constructor(client: CommandClient, base: Client) {
        super(client, base);
        this.label = 'info';
        this.options = {
            description: 'Get some info on the instance',
            fullDescription: 'Get some information related to the Evolve-X instance, or try to.',
            usage: 'info (instance URL)',
        };
        this.aliases = ['i'];
    }

    async execute(msg: Eris.Message, args: string[] ): Promise<Eris.Message> {
        let url;
        if (args[0] && args[0].match(/https:\/\/[a-z]+\.[a-z0-9]/) ) {
            url = args[0];
        }
        if (!this.base && !url) {
            return msg.channel.createMessage('Something died.');
        }
        if (!this.base || (!this.base.config.url && !url) ) {
            return msg.channel.createMessage('Instance not set up!');
        }
        if (this.base && !url) {
            // Basically object destructuring doesn't work if you are overwriting a variable that is already declared.
            // eslint-disable-next-line prefer-destructuring
            url = this.base.config.url;
        }
        let req;
        try {
            req = await superagent.get(`${url}/api`);
        } catch (e) {
            if ( (e.response && e.response.notFound) || (e.errno && (e.errno === 'ENOTFOUND' || e.errno === 'ECONNREFUSED') ) ) {
                return msg.channel.createMessage('Failed getting data. URL not found (possibly offline) or Evolve-X is below version 1.0. :/');
            }
            return msg.channel.createMessage(`Unknown error occurred: \`\`\`js\n${e}\`\`\``);
        }
        if (!req) {
            return msg.channel.createMessage(`Unknown error occurred`);
        }
        if (!['text/plain', 'application/json'].includes(req.type) ) {
            return msg.channel.createMessage('Invalid Content-Type received from server');
        }
        if (req.text === 'Pong!') {
            return msg.channel.createMessage('Evolve-X version is below V1.3.0, no data to be found.');
        }
        if (req.type !== 'application/json' && req.text !== 'Pong') {
            return msg.channel.createMessage('Received invalid response from server.');
        }
        const data = JSON.parse(req.text);
        const embed = {
            color: 0x15959D,
            title: 'Evolve-X Stats',
            fields: [
                {
                    name: 'Node Version',
                    value: data.nodeVersion,
                },
                {
                    name: 'Instance Version',
                    value: data.version,
                },
                {
                    name: 'Uptime',
                    value: data.uptime,
                },
                {
                    name: 'Online Since',
                    value: moment(data.onlineSince).format('dddd, MMMM Do YYYY, h:mm:ss a'),
                },
            ], description: 'Powered by Evolve-X and Evolve-X-Bot (Evolve-X add on type bot).',

        };
        if (data.shards) {
            embed.fields.push( { name: 'Shards', value: data.shards } );
        }
        if (this.base && this.base.config.showUrl) {
            embed.description += `\nEvolve-X Instance URL: ${this.base.config.url}`;
        }
        return msg.channel.createMessage( { embed } );
    }
}
