/**
 * @license
 *
 * EVX-Bot
 * Copyright (C) 2019  VoidNull
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Command from '../Structures/Command';
import Eris from 'eris';
import Client from '../Structures/Client';
import { inspect } from 'util';

export default class Eval extends Command {
    constructor(client: Eris.CommandClient, base: Client) {
        super(client, base);
        this.label = 'eval';
        this.aliases = ['e'];
        this.permissionsNeeded = ['sendMessages'];

        this.ownerOnly = true;
        this.options = {
            argsRequired: true,
            description: 'Eval code',
            usage: 'eval [code]',
            fullDescription: 'Eval some JavaScript code on the bots side',
        };
    }

    async execute(msg: Eris.Message, args: string[] ): Promise<Eris.Message|void> {
        if (!this.base) {
            return msg.channel.createMessage('Internal Error Occurred.');
        }
        let errored = 0;
        let evaled;
        try {
            // eslint-disable-next-line no-eval
            evaled = await eval(args.join(' ') );
            switch (typeof evaled) {
                case 'object': {
                    evaled = inspect(evaled, { depth: 0, showHidden: true } );
                    break;
                }
                default: {
                    evaled = String(evaled);
                }
            }

            if (evaled.length === 0 || !evaled) {
                errored = 1;
                return msg.channel.createMessage('No output!');
            }

            // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
            // @ts-ignore
            evaled = evaled.replace(this.client.token, 'NEIN');
            evaled = evaled.replace(this.base.config.token, 'NEIN');

            const msglimit = 2000;
            if (evaled.length > msglimit) {
                evaled = evaled.match(/[\s\S]{1,1900}[\n\r]/g) || [];
                try {
                    if (evaled.length >= 3) {
                        msg.channel.createMessage(`\`\`\`js\n${evaled[0]}\`\`\``);
                        msg.channel.createMessage(`\`\`\`js\n${evaled[1]}\`\`\``);
                        msg.channel.createMessage(`\`\`\`js\n${evaled[2]}\`\`\``);
                    } else {
                        evaled.forEach( (message: any) => {
                            msg.channel.createMessage(`\`\`\`js\n${message}\`\`\``);
                        } );
                    }
                    return Promise.resolve();
                } catch (err) {
                    errored = 1;
                    console.log('Something happened! See below');
                    console.error(err);
                    return Promise.resolve();
                }
            }
        } catch (err) {
            errored = 1;
            return msg.channel.createMessage(`Eval errored. Check below\`\`\`js\n${err.message || err}\`\`\``);
        }
        if (errored > 0) {
            return Promise.resolve();
        }
        return msg.channel.createMessage(`\`\`\`js\n${evaled}\`\`\``);
    }
}
