# Keys

token - The bot token to login with

url - The URL of the instance to monitor

owner - Object containing discord id and token auth.

owner.discordID - String, your ID

owner.tokenAuth - String formatted like `userID:token`

prefix - Bot prefix

showUrl - Whether or not to show the URL with the info command

# Example

```json
{
  "token": "Some discord bot token here",
  "url": "https://evx.wtf"
}
```
